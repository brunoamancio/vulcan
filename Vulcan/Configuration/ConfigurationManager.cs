﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using log4net;
using log4net.Config;
using Microsoft.Extensions.Configuration;
using Vulcan.Utils;

namespace Vulcan.Configuration
{
    public static class ConfigurationManager
    {
        public static VulcanConfiguration VulcanConfiguration;

        private static IConfigurationRoot ConfigurationRoot;

        public static void Configure()
        {
            if (VulcanConfiguration != null) return;

            ConfigureFromConfigFile();
            ConfigureLogger();
        }

        private static void ConfigureFromConfigFile()
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("vulcan.config.json", optional: false, reloadOnChange: false);

            ConfigurationRoot = configurationBuilder.Build();
            VulcanConfiguration = new VulcanConfiguration();

            FillNodeSection();
            FillNeighbourSection();
        }

        private static void FillNodeSection()
        {
            if (VulcanConfiguration == null) return;

            VulcanConfiguration.Node = ConfigurationRoot.GetSection(nameof(VulcanConfiguration.Node)).Get<Node>();
        }

        private static void FillNeighbourSection()
        {
            if (VulcanConfiguration == null) return;

            VulcanConfiguration.Neighbors = ConfigurationRoot.GetSection(nameof(VulcanConfiguration.Neighbors)).Get<List<string>>();
        }

        private static void ConfigureLogger()
        {
            var log = LogManager.GetLogger(typeof(ConfigurationManager));
            
            var log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));

            var repo = LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

            XmlConfigurator.Configure(repo, log4netConfig["log4net"]);

            log.Info($"{Constants.APP_NAME} v{Constants.APP_VERSION} started");
        }
    }

    public class VulcanConfiguration
    {
        public Node Node { get; set; }
        public IEnumerable<string> Neighbors { get; set; }
    }

    public class Node
    {
        public uint UDPPort { get; set; }
        public uint TCPPort { get; set; }
    }
}