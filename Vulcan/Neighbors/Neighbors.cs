﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vulcan.DTOs;
using Vulcan.Neighbors.Network;
using Vulcan.Utils;
using Vulcan.Utils.Extensions;

namespace Vulcan.Neighbors
{
    public class Neighbor : ISender<IEnumerable<byte>>, ISender<Transaction>
    {
        public readonly NetworkConstants.NetworkProtocol Protocol;
        public readonly string Hostname;
        public string IPAddress { get; }
        public readonly uint Port;
        
        private readonly INetworkClient networkClient;

        public Neighbor(string addressWithPort)
        {
            var uri = new Uri(addressWithPort);
            var success = uint.TryParse(uri.Port.ToString(), out var port);
            if (!success) throw new ArgumentException($"{nameof(addressWithPort)} port is invalid");

            var successProtocolParse = Enum.TryParse(uri.Scheme, true, out Protocol);
            if(!successProtocolParse)
                throw new ArgumentException($"Protocol \"{uri.Scheme}\" is invalid");

            var isIP = uri.HostNameType == UriHostNameType.IPv4 || uri.HostNameType == UriHostNameType.IPv6;
            IPAddress = isIP ? uri.DnsSafeHost : NetworkHelper.ResolveDNS(uri.DnsSafeHost);
            Hostname = isIP ? IPAddress : uri.DnsSafeHost;
            Port = port;

            if (string.IsNullOrWhiteSpace(IPAddress)) return;

            networkClient = GetNetworkClient();
            networkClient.Connect();
        }

        private INetworkClient GetNetworkClient()
        {
            return networkClient ?? NetworkHelper.GenerateNetworkClient(Protocol, IPAddress, Port);
        }

        public void Send(Transaction tx)
        {
            if (networkClient == null) return;

            var txBytes = tx.ToBytes();
            Send(txBytes);
        }

        public void Send(IEnumerable<byte> data)
        {
            if (networkClient == null) return;

            var dataToSend = AppendTransactionToRequest(data);
            networkClient?.Send(dataToSend);
        }

        private IEnumerable<byte> AppendTransactionToRequest(IEnumerable<byte> txBytes)
        {
            var txToRequest = new byte[Constants.REQ_HASH_SIZE]; // TODO Select a tx to request (use IEnumerable!)

            var dataToSend = new byte[NetworkConstants.NETWORK_PACKET_SIZE];
            Buffer.BlockCopy(txBytes.ToArray(), 0, dataToSend, 0, txBytes.Count());
            Buffer.BlockCopy(txToRequest.ToArray(), 0, dataToSend, Constants.TX_SIZE_BYTES, txToRequest.Count());

            return dataToSend;
        }
    }
}
