﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Vulcan.Configuration;
using Vulcan.Neighbors.Network.TCP;
using Vulcan.Neighbors.Network.UDP;

namespace Vulcan.Neighbors.Network
{
    public class NetworkHelper
    {
        public static bool IsValidPort(uint port)
        {
            return port != 0 && port <= 65535;
        }

        public static string ResolveDNS(string uriDnsSafeHost)
        {
            return Dns.GetHostAddresses(uriDnsSafeHost).FirstOrDefault()?.ToString() ?? "";
        }

        public static INetworkClient GenerateNetworkClient(NetworkConstants.NetworkProtocol protocol, string serverAddress, uint serverPort)
        {
            switch (protocol)
            {
                case NetworkConstants.NetworkProtocol.UDP:
                    return new UDPClient(serverAddress, serverPort);
                case NetworkConstants.NetworkProtocol.TCP:
                    return new TCPClient(serverAddress, serverPort);
                default:
                    throw new NotImplementedException($"{protocol} is not a supported protocol.");
            }
        }

        public static NetworkServer GenerateNetworkServer(NetworkConstants.NetworkProtocol protocol, 
            Action<IEnumerable<byte>, IEnumerable<byte>, IPAddress, uint, INetworkServer> onDataReceived)
        {
            switch (protocol)
            {
                case NetworkConstants.NetworkProtocol.UDP:
                    return new UDPServer(ConfigurationManager.VulcanConfiguration.Node.UDPPort, onDataReceived);
                case NetworkConstants.NetworkProtocol.TCP:
                    return new TCPServer(ConfigurationManager.VulcanConfiguration.Node.TCPPort, onDataReceived);
                default:
                    throw new NotImplementedException($"{protocol} is not a supported protocol.");
            }
        }
    }
}
