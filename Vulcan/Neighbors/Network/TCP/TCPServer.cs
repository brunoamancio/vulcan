﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Vulcan.Neighbors.Network.TCP
{
    public class TCPServer : NetworkServer
    {
        private readonly TcpListener tcpServer;
        
        public TCPServer(uint port, Action<IEnumerable<byte>, IEnumerable<byte>, IPAddress, uint, INetworkServer> onDataReceived)
            : base(port, onDataReceived)
        {
            tcpServer = new TcpListener(IPAddress.Any, serverEndPoint.Port);
        }

        public override async void Listen()
        {
            if (IsActive) return;

            tcpServer.Stop();
            tcpServer.Start();
            Activate();

            while (IsActive)
            {
                try
                {
                    await tcpServer.AcceptTcpClientAsync()
                                   .ContinueWith(t => {
                                        var tcpClient = t.Result;
                                        using (var networkStream = tcpClient.GetStream())
                                        {
                                            var dataReceived = new byte[NetworkConstants.NETWORK_PACKET_SIZE];
                                            networkStream.Read(dataReceived, 0, NetworkConstants.NETWORK_PACKET_SIZE);
                                            if (!ValidateDataReceived(dataReceived)) return;

                                            var senderIpEndpoint = (IPEndPoint)tcpClient.Client.RemoteEndPoint;
                                            var senderIpAddress = senderIpEndpoint.Address;
                                            var senderPort = (uint)senderIpEndpoint.Port;

                                            PrepareAndRunOnDataReceived(dataReceived, senderIpAddress, senderPort);
                                        }
                                   });
                }
                catch (Exception e)
                {
                    //If server has been disposed of, do nothing
                    if (e is AggregateException && (e.InnerException is ObjectDisposedException || e.InnerException is SocketException))
                        return;

                    throw;
                }
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            tcpServer.Stop();
        }
    }
}
