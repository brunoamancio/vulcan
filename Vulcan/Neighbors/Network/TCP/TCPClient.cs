﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Vulcan.Neighbors.Network.TCP
{
    public class TCPClient : INetworkClient
    {
        private readonly TcpClient tcpClient;

        public TCPClient(string serverAddress, uint serverPort)
        {
            var isValidIP = IPAddress.TryParse(serverAddress, out _);
            if (!isValidIP)
                throw new ArgumentException($"{nameof(serverAddress)} \"{serverAddress}\" is invalid");

            if (!NetworkHelper.IsValidPort(serverPort))
                throw new ArgumentException($"{nameof(serverPort)} \"{serverPort}\" is invalid.");

            tcpClient = new TcpClient(serverAddress, Convert.ToInt32(serverPort));
        }

        public void Connect()
        {
            // No action needed
        }

        public void Send(IEnumerable<byte> data)
        {
            using (var nwStream = tcpClient.GetStream())
                nwStream.Write(data.ToArray(), 0, data.Count());
        }
    }
}