﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Vulcan.Neighbors.Network.UDP
{
    public class UDPClient : INetworkClient
    {
        private readonly UdpClient client = new UdpClient();
        private IPEndPoint serverIpEndPoint; // TODO: Implement a check every X minutes to check if IP changed.

        public UDPClient(string serverAddress, uint serverPort)
        {
            var isValidIP = IPAddress.TryParse(serverAddress, out var serverIpAddress);
            if (!isValidIP)
                throw new ArgumentException($"{nameof(serverAddress)} \"{serverAddress}\" is invalid");

            if (!NetworkHelper.IsValidPort(serverPort))
                throw new ArgumentException($"{nameof(serverPort)} \"{serverPort}\" is invalid.");

            serverIpEndPoint = new IPEndPoint(serverIpAddress, Convert.ToInt32(serverPort));
        }

        public void Connect()
        {
            client.Connect(serverIpEndPoint);
        }
        
        public void Send(IEnumerable<byte> data)
        {
            if (!data?.Any() ?? true) return;
            
            client.Send(data.ToArray(), data.Count());
        }

        public void Dispose()
        {
            client?.Dispose();
        }
    }
}