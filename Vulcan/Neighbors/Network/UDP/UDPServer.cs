﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Vulcan.Neighbors.Network.UDP
{
    public class UDPServer : NetworkServer
    {
        private UdpClient udpServer;

        public UDPServer(uint port, Action<IEnumerable<byte>, IEnumerable<byte>, IPAddress, uint, INetworkServer> onDataReceived)
            : base(port, onDataReceived)
        {
            udpServer = new UdpClient(serverEndPoint.Port);
        }

        public override async void Listen()
        {
            if (IsActive) return;

            Activate();

            while (IsActive)
            {
                try
                {
                    await udpServer.ReceiveAsync()
                                   .ContinueWith(t => {
                                        var dataReceived = t.Result.Buffer;
                                        if (!ValidateDataReceived(dataReceived)) return;

                                        var senderIpAddress = t.Result.RemoteEndPoint.Address;
                                        var senderPort = (uint)t.Result.RemoteEndPoint.Port;

                                        PrepareAndRunOnDataReceived(dataReceived, senderIpAddress, senderPort);
                                   });
                }
                catch (Exception e)
                {
                    //If server has been disposed of, do nothing
                    if (e is AggregateException && (e.InnerException is ObjectDisposedException || e.InnerException is SocketException))
                        return;
                    
                    throw;
                }
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            udpServer.Dispose();
        }
    }
}
