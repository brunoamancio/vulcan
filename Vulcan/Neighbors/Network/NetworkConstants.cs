﻿namespace Vulcan.Neighbors.Network
{
    public static class NetworkConstants
    {
        public enum NetworkProtocol
        {
            UDP,
            TCP
        }

        public const int NETWORK_PACKET_SIZE = 1650;
    }
}
