﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Vulcan.Utils;

namespace Vulcan.Neighbors.Network
{
    public abstract class NetworkServer : INetworkServer
    {
        public bool IsActive { get; private set; }

        public uint Port { get; }

        protected IPEndPoint serverEndPoint;

        protected readonly Action<IEnumerable<byte>, IEnumerable<byte>, IPAddress, uint, INetworkServer> onDataReceived;
        
        protected NetworkServer(uint port, Action<IEnumerable<byte>, IEnumerable<byte>, IPAddress, uint, INetworkServer> onDataReceived)
        {
            if (!NetworkHelper.IsValidPort(port))
                throw new ArgumentException($"{nameof(port)} {port} is invalid.");

            this.onDataReceived = onDataReceived ?? throw new ArgumentException($"{nameof(onDataReceived)} is invalid.");

            serverEndPoint = new IPEndPoint(IPAddress.Any, Convert.ToInt32(port));
            Port = port;
        }
        
        public abstract void Listen();

        protected bool ValidateDataReceived(IEnumerable<byte> dataReceived)
        {
            return dataReceived.Count() == NetworkConstants.NETWORK_PACKET_SIZE;
        }

        protected void PrepareAndRunOnDataReceived(byte[] dataReceived, IPAddress senderIpAddress, uint senderPort)
        {
            var txData = new byte[Constants.TX_SIZE_BYTES];
            Buffer.BlockCopy(dataReceived, 0, txData, 0, Constants.TX_SIZE_BYTES);

            var txRequestedBySender = new byte[Constants.REQ_HASH_SIZE];
            Buffer.BlockCopy(dataReceived, Constants.TX_SIZE_BYTES, txRequestedBySender, 0, Constants.REQ_HASH_SIZE);

            onDataReceived.Invoke(txData, txRequestedBySender, senderIpAddress, senderPort, this);
        }

        public void Restart()
        {
            if (!IsActive) return;

            Dispose();
            Listen(); // Will cause issue when restarting if server object is not restarted properly
        }

        protected void Activate()
        {
            IsActive = true;
        }

        protected void Deactivate()
        {
            IsActive = false;
        }

        public virtual void Dispose()
        {
            Deactivate();
        }
    }
}