﻿using System;
using System.Collections.Generic;

namespace Vulcan.Neighbors.Network
{
    public interface INetworkServer : IDisposable
    {
        void Listen();
        
        void Restart();
    }

    public interface INetworkClient : ISender<IEnumerable<byte>>
    {
        void Connect();
    }

    public interface ISender<in T>
    {
        void Send(T data);
    }

    public interface IReceiver<out T>
    {
        T Receive();
    }
}