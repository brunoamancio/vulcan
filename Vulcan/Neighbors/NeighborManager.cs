﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using log4net;
using Vulcan.Configuration;
using Vulcan.Neighbors.Network;
using Vulcan.Utils.Extensions;

namespace Vulcan.Neighbors
{
    public static class NeighborManager
    {
        private static IEnumerable<string> ConfiguredNeighborAddressesWithPort => ConfigurationManager.VulcanConfiguration.Neighbors;

        private static readonly IList<Neighbor> ActiveNeighbors = new List<Neighbor>();

        public static ILog log = LogManager.GetLogger(typeof(NeighborManager));

        public static void Start()
        {
            ActiveNeighbors.Clear();

            foreach (var configuredNeighborAddressWithPort in ConfiguredNeighborAddressesWithPort)
            {
                var neighbor = new Neighbor(configuredNeighborAddressWithPort);
                ActiveNeighbors.Add(neighbor);
            }

            StartListening();
        }

        private static async void StartListening()
        {
            foreach (var protocol in Enum.GetValues(typeof(NetworkConstants.NetworkProtocol))
                                         .Cast<NetworkConstants.NetworkProtocol>())
            {
                var server = NetworkHelper.GenerateNetworkServer(protocol, OnDataReceived);
                if (!server.IsActive)
                    await Task.Run(() =>
                    {
                        log.Info($"Started listening for neighbors messages on {protocol}");
                        server.Listen();
                    });
            }
        }

        public static void OnDataReceived(IEnumerable<byte> txDataReceived, IEnumerable<byte> txRequestedBySender, 
                                          IPAddress senderIPAddress, uint senderPort, INetworkServer server) 
        {
            var senderNeighbor = GetNeighborByPort(senderIPAddress, senderPort);
            var isSenderActiveNeighbor = senderNeighbor != null;
            if (!isSenderActiveNeighbor) return;

            log.Debug($"Received TX from {senderNeighbor.Hostname}:{senderPort}");

            // TODO take fingerprint
            //Check if it exists in DB
            var tryteStringReceivedFromClient = txDataReceived.ToTransactionTrytes();
            //senderNeighbor.Send(txDataReceived); // Asnwer
        }

        public static Neighbor GetNeighborByPort(this IPAddress neighborsIP, uint senderPort)
        {
            if (senderPort == 0) return null;

            return ActiveNeighbors.FirstOrDefault(t => {
                var neighborsIpString = neighborsIP.ToString();
                return t.IPAddress == neighborsIpString && t.Port == senderPort;
            });
        }
    }
}