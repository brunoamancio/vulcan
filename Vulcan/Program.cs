﻿using System;
using Vulcan.Configuration;
using Vulcan.Neighbors;

namespace Vulcan
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Node started - Press enter to close");

            Initialize();
            
            Console.ReadLine();
        }

        private static void Initialize()
        {
            ConfigurationManager.Configure();
            NeighborManager.Start();
        }
    }
}
