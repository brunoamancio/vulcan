﻿using System.Collections.Generic;
using Vulcan.DTOs;

namespace Vulcan.Utils.Extensions
{
    public static class TrytesExtension
    {
        public static IEnumerable<byte> ToBytes(this ITrytes trytes)
        {
            var trytesToConvert = trytes.GetTrytes();
            return Converter.TrytesToBytes(trytesToConvert);
        }
    }
}