﻿using System.Collections.Generic;

namespace Vulcan.Utils.Extensions
{
    public static class StringExtension
    {
        public static IEnumerable<byte> ToBytes(this string trytes)
        {
            return Converter.TrytesToBytes(trytes);
        }
    }
}