﻿using System.Collections.Generic;

namespace Vulcan.Utils.Extensions
{
    public static class ByteExtension
    {
        public static IEnumerable<sbyte> ToTrits(this IEnumerable<byte> bytes)
        {
            var trits = Converter.BytesToTrits(bytes);
            return trits;
        }

        public static string ToTrytes(this IEnumerable<byte> bytes, ushort maxLength = 0)
        {
            var trits = ToTrits(bytes);
            var trytes = Converter.TritsToTrytes(trits, maxLength);

            return trytes;
        }

        public static string ToTransactionTrytes(this IEnumerable<byte> bytes)
        {
            var trytes = ToTrytes(bytes, Constants.TX_SIZE_TRYTES);
            return trytes;
        }
    }
}
