﻿using Vulcan.Neighbors.Network;

namespace Vulcan.Utils
{
    public static class Constants
    {
        public const string APP_NAME = "Vulcan";
        public const string APP_VERSION = "0.1alpha";

        public const int TX_SIZE_TRYTES = 2673;
        public const int REQ_HASH_SIZE = 46; // Neighbors uses these bytes for requesting TXs
        public const int TX_SIZE_BYTES = NetworkConstants.NETWORK_PACKET_SIZE - REQ_HASH_SIZE;
    }
}
