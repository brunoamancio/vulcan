﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vulcan.DTOs;

namespace Vulcan.Utils
{
    public static class Converter
    {
        private static readonly Dictionary<char, sbyte[]> TrytesLookup = new Dictionary<char, sbyte[]>
        {
            { '9', new sbyte[] { 0, 0, 0 } },
            { 'A', new sbyte[] { 1, 0, 0 } },
            { 'B', new sbyte[] { -1, 1, 0 } },
            { 'C', new sbyte[] { 0, 1, 0 } },
            { 'D', new sbyte[] { 1, 1, 0 } },
            { 'E', new sbyte[] { -1, -1, 1 } },
            { 'F', new sbyte[] { 0, -1, 1 } },
            { 'G', new sbyte[] { 1, -1, 1 } },
            { 'H', new sbyte[] { -1, 0, 1 } },
            { 'I', new sbyte[] { 0, 0, 1 } },
            { 'J', new sbyte[] { 1, 0, 1 } },
            { 'K', new sbyte[] { -1, 1, 1 } },
            { 'L', new sbyte[] { 0, 1, 1 } },
            { 'M', new sbyte[] { 1, 1, 1 } },
            { 'N', new sbyte[] { -1, -1, -1 } },
            { 'O', new sbyte[] { 0, -1, -1 } },
            { 'P', new sbyte[] { 1, -1, -1 } },
            { 'Q', new sbyte[] { -1, 0, -1 } },
            { 'R', new sbyte[] { 0, 0, -1 } },
            { 'S', new sbyte[] { 1, 0, -1 } },
            { 'T', new sbyte[] { -1, 1, -1 } },
            { 'U', new sbyte[] { 0, 1, -1 } },
            { 'V', new sbyte[] { 1, 1, -1 } },
            { 'W', new sbyte[] { -1, -1, 0 } },
            { 'X', new sbyte[] { 0, -1, 0 } },
            { 'Y', new sbyte[] { 1, -1, 0 } },
            { 'Z', new sbyte[] { -1, 0, 0 } }
        };

        private static readonly sbyte[] BYTES_TO_TRITS =
        {
            0, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, -1, -1, 1, 0, 0, 0, -1, 1, 0, 0,
            1, -1, 1, 0, 0, -1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, -1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0,
            -1, -1, -1, 1, 0, 0, -1, -1, 1, 0, 1, -1, -1, 1, 0, -1, 0, -1, 1, 0, 0, 0, -1, 1, 0, 1, 0, -1, 1, 0, -1, 1,
            -1, 1, 0, 0, 1, -1, 1, 0, 1, 1, -1, 1, 0, -1, -1, 0, 1, 0, 0, -1, 0, 1, 0, 1, -1, 0, 1, 0, -1, 0, 0, 1, 0,
            0, 0, 0, 1, 0, 1, 0, 0, 1, 0, -1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, -1, -1, 1, 1, 0, 0, -1, 1, 1, 0,
            1, -1, 1, 1, 0, -1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, -1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0,
            -1, -1, -1, -1, 1, 0, -1, -1, -1, 1, 1, -1, -1, -1, 1, -1, 0, -1, -1, 1, 0, 0, -1, -1, 1, 1, 0, -1, -1, 1,
            -1, 1, -1, -1, 1, 0, 1, -1, -1, 1, 1, 1, -1, -1, 1, -1, -1, 0, -1, 1, 0, -1, 0, -1, 1, 1, -1, 0, -1, 1, -1,
            0, 0, -1, 1, 0, 0, 0, -1, 1, 1, 0, 0, -1, 1, -1, 1, 0, -1, 1, 0, 1, 0, -1, 1, 1, 1, 0, -1, 1, -1, -1, 1, -1,
            1, 0, -1, 1, -1, 1, 1, -1, 1, -1, 1, -1, 0, 1, -1, 1, 0, 0, 1, -1, 1, 1, 0, 1, -1, 1, -1, 1, 1, -1, 1, 0, 1,
            1, -1, 1, 1, 1, 1, -1, 1, -1, -1, -1, 0, 1, 0, -1, -1, 0, 1, 1, -1, -1, 0, 1, -1, 0, -1, 0, 1, 0, 0, -1, 0,
            1, 1, 0, -1, 0, 1, -1, 1, -1, 0, 1, 0, 1, -1, 0, 1, 1, 1, -1, 0, 1, -1, -1, 0, 0, 1, 0, -1, 0, 0, 1, 1, -1,
            0, 0, 1, -1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, -1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, -1, -1,
            1, 0, 1, 0, -1, 1, 0, 1, 1, -1, 1, 0, 1, -1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, -1, 1, 1, 0, 1, 0, 1,
            1, 0, 1, 1, 1, 1, 0, 1, -1, -1, -1, 1, 1, 0, -1, -1, 1, 1, 1, -1, -1, 1, 1, -1, 0, -1, 1, 1, 0, 0, -1, 1, 1,
            1, 0, -1, 1, 1, -1, 1, -1, 1, 1, 0, 1, -1, 1, 1, 1, 1, -1, 1, 1, -1, -1, 0, 1, 1, 0, -1, 0, 1, 1, 1, -1, 0,
            1, 1, -1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, -1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, -1, -1, 1,
            1, 1, 0, -1, 1, 1, 1, 1, -1, 1, 1, 1, -1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, -1, 1, 1, 1, 1, 0, 1, 1,
            1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, 0, -1, -1, -1, 0, 0, -1,
            -1, -1, 1, 0, -1, -1, -1, -1, 1, -1, -1, -1, 0, 1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, 0, -1, -1, 0, -1,
            0, -1, -1, 1, -1, 0, -1, -1, -1, 0, 0, -1, -1, 0, 0, 0, -1, -1, 1, 0, 0, -1, -1, -1, 1, 0, -1, -1, 0, 1, 0,
            -1, -1, 1, 1, 0, -1, -1, -1, -1, 1, -1, -1, 0, -1, 1, -1, -1, 1, -1, 1, -1, -1, -1, 0, 1, -1, -1, 0, 0, 1,
            -1, -1, 1, 0, 1, -1, -1, -1, 1, 1, -1, -1, 0, 1, 1, -1, -1, 1, 1, 1, -1, -1, -1, -1, -1, 0, -1, 0, -1, -1,
            0, -1, 1, -1, -1, 0, -1, -1, 0, -1, 0, -1, 0, 0, -1, 0, -1, 1, 0, -1, 0, -1, -1, 1, -1, 0, -1, 0, 1, -1, 0,
            -1, 1, 1, -1, 0, -1, -1, -1, 0, 0, -1, 0, -1, 0, 0, -1, 1, -1, 0, 0, -1, -1, 0, 0, 0, -1, 0, 0, 0, 0, -1, 1,
            0, 0, 0, -1, -1, 1, 0, 0, -1, 0, 1, 0, 0, -1, 1, 1, 0, 0, -1, -1, -1, 1, 0, -1, 0, -1, 1, 0, -1, 1, -1, 1,
            0, -1, -1, 0, 1, 0, -1, 0, 0, 1, 0, -1, 1, 0, 1, 0, -1, -1, 1, 1, 0, -1, 0, 1, 1, 0, -1, 1, 1, 1, 0, -1, -1,
            -1, -1, 1, -1, 0, -1, -1, 1, -1, 1, -1, -1, 1, -1, -1, 0, -1, 1, -1, 0, 0, -1, 1, -1, 1, 0, -1, 1, -1, -1,
            1, -1, 1, -1, 0, 1, -1, 1, -1, 1, 1, -1, 1, -1, -1, -1, 0, 1, -1, 0, -1, 0, 1, -1, 1, -1, 0, 1, -1, -1, 0,
            0, 1, -1, 0, 0, 0, 1, -1, 1, 0, 0, 1, -1, -1, 1, 0, 1, -1, 0, 1, 0, 1, -1, 1, 1, 0, 1, -1, -1, -1, 1, 1, -1,
            0, -1, 1, 1, -1, 1, -1, 1, 1, -1, -1, 0, 1, 1, -1, 0, 0, 1, 1, -1, 1, 0, 1, 1, -1, -1, 1, 1, 1, -1, 0, 1, 1,
            1, -1, 1, 1, 1, 1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, 0, 1, -1, -1, -1, 0, -1, 0, -1, -1, 0, 0, 0, -1,
            -1, 0, 1, 0, -1, -1, 0, -1, 1, -1, -1, 0, 0, 1, -1, -1, 0, 1, 1, -1, -1, 0, -1, -1, 0, -1, 0, 0, -1, 0, -1,
            0, 1, -1, 0, -1, 0, -1, 0, 0, -1, 0, 0, 0, 0, -1, 0, 1, 0, 0, -1, 0, -1, 1, 0, -1, 0, 0, 1, 0, -1, 0, 1, 1,
            0, -1, 0, -1, -1, 1, -1, 0, 0, -1, 1, -1, 0, 1, -1, 1, -1, 0, -1, 0, 1, -1, 0, 0, 0, 1, -1, 0, 1, 0, 1, -1,
            0, -1, 1, 1, -1, 0, 0, 1, 1, -1, 0, 1, 1, 1, -1, 0, -1, -1, -1, 0, 0, 0, -1, -1, 0, 0, 1, -1, -1, 0, 0, -1,
            0, -1, 0, 0, 0, 0, -1, 0, 0, 1, 0, -1, 0, 0, -1, 1, -1, 0, 0, 0, 1, -1, 0, 0, 1, 1, -1, 0, 0, -1, -1, 0, 0,
            0, 0, -1, 0, 0, 0, 1, -1, 0, 0, 0, -1, 0, 0, 0, 0
        };

        public static IEnumerable<sbyte> BytesToTrits(IEnumerable<byte> bytes)
        {
            var size = bytes.Count();
            var trits = new List<sbyte>();

            for (var i = 0; i < size; i++)
            {
                var v = (int)bytes.ElementAt(i);
                var w = (sbyte)bytes.ElementAt(i);
                if (w < 0)
                    v -= 13;

                var tableOffset = v * 5;

                trits.Add(BYTES_TO_TRITS[tableOffset]); 
                trits.Add(BYTES_TO_TRITS[tableOffset + 1]);
                trits.Add(BYTES_TO_TRITS[tableOffset + 2]);
                trits.Add(BYTES_TO_TRITS[tableOffset + 3]);
                trits.Add(BYTES_TO_TRITS[tableOffset + 4]);
            }

            return trits;
        }

        public static string TritsToTrytes(IEnumerable<sbyte> trits, ushort maxLength = 0)
        {
            var trytes = new StringBuilder();

            for (var i = 0; i < trits.Count(); i+=3)
            {
                if (maxLength > 0 && trytes.Length == maxLength) break;

                var tryte = TrytesLookup.First(t => t.Value[0] == trits.ElementAtOrDefault(i)
                                                    && t.Value[1] == trits.ElementAtOrDefault(i + 1)
                                                    && t.Value[2] == trits.ElementAtOrDefault(i + 2))
                    .Key.ToString();

                trytes.Append(tryte);
            }
            
            return trytes.ToString();
        }

        public static IEnumerable<byte> TrytesToBytes(string trytes)
        {
            var trits = TrytesToTrits(trytes);
            var bytes = TritsToBytes(trits);
            return bytes;
        }

        public static IEnumerable<sbyte> TrytesToTrits(string trytes)
        {
            var trits = new List<sbyte>();
            if (string.IsNullOrWhiteSpace(trytes)) return trits;

            try
            {
                foreach (var tryte in trytes)
                {
                    var trit = TrytesLookup.First(t => t.Key == tryte).Value;
                    trits.AddRange(trit);
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException("Error on trytes to trits conversion.", e);
            }

            return trits;
        }

        public static IEnumerable<byte> TrytesToBytes(Transaction transaction)
        {
            return null;
        }

        public static IEnumerable<byte> TritsToBytes(IEnumerable<sbyte> trits)
        {
            var bytes = new List<byte>();

            var length = trits.Count();
            var size = Math.Ceiling((decimal)length/5);
            
            for(var i = 0; i < size; i++)
            {
                var indexMultiplier1 = i * 5;
                var indexMultiplier2 = indexMultiplier1 + 1;
                var indexMultiplier3 = indexMultiplier1 + 2;
                var indexMultiplier4 = indexMultiplier1 + 3;
                var indexMultiplier5 = indexMultiplier1 + 4;

                var value1 = indexMultiplier1 >= length ? 0 : trits.ElementAt(indexMultiplier1);
                var value2 = indexMultiplier2 >= length ? 0 : trits.ElementAt(indexMultiplier2) * 3;
                var value3 = indexMultiplier3 >= length ? 0 : trits.ElementAt(indexMultiplier3) * 9;
                var value4 = indexMultiplier4 >= length ? 0 : trits.ElementAt(indexMultiplier4) * 27;
                var value5 = indexMultiplier5 >= length ? 0 : trits.ElementAt(indexMultiplier5) * 81;

                bytes.Add((byte)(value1 + value2 + value3 + value4 + value5));
            }

            return bytes;
        }
    }
}
