﻿namespace Vulcan.DTOs
{
    public interface ITrytes
    {
        int MaxLength { get; }

        string GetTrytes();
    }
}