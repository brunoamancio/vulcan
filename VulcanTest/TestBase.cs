﻿using NUnit.Framework;
using Vulcan.Configuration;

namespace VulcanTest
{
    public class TestBase
    {
        [OneTimeSetUp]
        public void FixtureSetUp()
        {
            ConfigurationManager.Configure();
        }
    }
}