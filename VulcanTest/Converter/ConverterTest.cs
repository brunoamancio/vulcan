﻿using NUnit.Framework;

namespace VulcanTest.Converter
{
    public class ConverterTest
    {
        [Test(Description = "Trits to Bytes convert 5 trits into 1 byte. When length isnt multiple of 5, the extra trits are ignored")]
        public void TritsToBytes_NoPadding()
        {
            // Arrange
            var trits = new sbyte[]{ 1, 1, -1, 0, 1, -1, 0, 1, 1, 0, 1, 0, 1, 0, 0, -1, -1, -1 };

            // Act
            var result = Vulcan.Utils.Converter.TritsToBytes(trits);

            // Assert
            var expected = new byte[]{ 76, 35, 10, 243 };
            Assert.AreEqual(expected, result, "Bytes wrong!");
        }

        [Test(Description = "Bytes to trits will always pad to have length multiple of 5")]
        public void BytesToTrits()
        {
            // Arrange
            var bytes = new byte[] { 76, 35, 10, 243 };

            // Act
            var result = Vulcan.Utils.Converter.BytesToTrits(bytes);

            // Assert
            var expected = new sbyte[] { 1, 1, -1, 0, 1, -1, 0, 1, 1, 0, 1, 0, 1, 0, 0, -1, -1, -1, 0, 0 };
            Assert.AreEqual(expected, result, "Trits wrong!");
        }

        [Test]
        public void TritsToTrytes()
        {
            // Arrange
            const string trytes = "VULCAN";
            
            // Act
            var result = Vulcan.Utils.Converter.TrytesToTrits(trytes);

            // Assert 
            var expected = new sbyte[] { 1, 1, -1, 0, 1, -1, 0, 1, 1, 0, 1, 0, 1, 0, 0, -1, -1, -1 };
            Assert.AreEqual(expected, result, "Trits wrong!");
        }

        [Test]
        public void TrytesToTrits_NoPadding()
        {
            // Arrange
            var trits = new sbyte[] { 1, 1, -1, 0, 1, -1, 0, 1, 1, 0, 1, 0, 1, 0, 0, -1, -1, -1 };
            
            // Act
            var result = Vulcan.Utils.Converter.TritsToTrytes(trits);

            // Assert 
            const string expected = "VULCAN";
            Assert.AreEqual(expected, result, "Trytes wrong!");
        }

        [Test]
        public void TrytesToTrits_WithPadding()
        {
            // Arrange
            var trits = new sbyte[] { 1, 1, -1, 0, 1, -1, 0, 1, 1, 0, 1, 0, 1, 0, 0, -1, -1, -1, 0 };

            // Act
            var result = Vulcan.Utils.Converter.TritsToTrytes(trits);

            // Assert 
            const string expected = "VULCAN9";
            Assert.AreEqual(expected, result, "Trytes wrong!");
        }

        [Test]
        public void TrytesToBytes()
        {
            // Arrange
            const string trytes = "VULCAN";

            // Act
            var result = Vulcan.Utils.Converter.TrytesToBytes(trytes);

            // Assert
            var expected = new byte[]{ 76, 35, 10, 243 };
            Assert.AreEqual(expected, result);
        }
    }
}
