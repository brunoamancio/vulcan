﻿namespace VulcanTest
{
    public static class TestConstants
    {
        public const string ServerIpAddress = "127.0.0.1";
        public const uint Port = 10000;
    }
}