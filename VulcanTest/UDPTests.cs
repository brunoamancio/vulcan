using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Vulcan.Neighbors.Network;
using Vulcan.Neighbors.Network.UDP;
using NUnit.Framework;

namespace VulcanTest
{
    public class UDPTests
    {
        [Test]
        public void Test_Client_Server_Communication_Ping_Pong()
        {
            // Arrange
            var dataToSendToServer = new byte[NetworkConstants.NETWORK_PACKET_SIZE];

            var received = false;
            var resultMatches = false;
            var onReceivedFromClient = new Action<IEnumerable<byte>, IEnumerable<byte>, IPAddress, uint, INetworkServer>(
                (receivedFromClient, txRequestedBySender, senderIPAddress, senderPort, networkServer) => {
                    var allDataReceived = receivedFromClient.Concat(txRequestedBySender);

                    // Assert 
                    try
                    {
                        Assert.AreEqual(dataToSendToServer, allDataReceived, "Received wrong message");
                        resultMatches = true;
                    }
                    catch (AssertionException) { }
                    
                    received = true;
                });

            using (var udpServer = new UDPServer(TestConstants.Port, onReceivedFromClient))
            {
                Task.Run(() => udpServer.Listen());

                while (!udpServer.IsActive)
                    Thread.Sleep(50); // Wait for server to be up

                var udpClient = new UDPClient(TestConstants.ServerIpAddress, TestConstants.Port);
                udpClient.Connect();

                // Act
                udpClient.Send(dataToSendToServer);

                while (!received)
                    Thread.Sleep(80);
            }
            
            
            // Assert (defined in onReceivedFromClient)
            Assert.IsTrue(resultMatches);
        }
    }
}